package email;

import javax.mail.Session;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : javaMail
 * @date : 12/11/15
 */
public final class SMTPSession {

    private static final Logger log = Logger.getLogger(SMTPSession.class.getName());

    private SMTPSession() {

    }

    /**
     * Creates an SMTP session for.
     *
     * @param username login username for the session.
     * @param password login password for the session.
     * @return an SMTP session.
     */
    public static Session getSMTPSession(String username, String password) {

        Properties connectionProperties = System.getProperties();

        connectionProperties.put("mail.smtp.auth", "true");
        connectionProperties.put("mail.smtp.starttls.enable", "true");
        connectionProperties.put("mail.smtp.host", "smtp.gmail.com");
        connectionProperties.put("mail.smtp.port", "587");

        connectionProperties.setProperty("mail.user", username);
        connectionProperties.setProperty("mail.password", password);

        //Step 2: Establish a mail session (java.mail.Session)
        Session smtpSession = Session.getDefaultInstance(connectionProperties);

        return smtpSession;
    }
}
