package users.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : javaServlet
 * @date : 08/12/15
 */

/**
 * Represents a SQLite JDBC connection.
 */
public final class SQLiteConnection {

    private static final Logger log = Logger.getLogger(SQLiteConnection.class.getName());

    private SQLiteConnection() {
    }

    /**
     * Creates a SQLite JDBC connection.
     *
     * @return a connection to a specified database.
     */
    public static Connection getConnection() {
        final String dbPath = "/home/saif/MEGA/uniStuff/sem3/ssc/assignments/javaServlet/resources/emailClient.db";

        Connection dbConn = null;

        try {
            Class.forName("org.sqlite.JDBC");
            dbConn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);

            if (dbConn != null) {
                log.log(Level.INFO, "Database accessed!");
            } else {
                log.log(Level.SEVERE, "Failed to make dbConnection");
            }
        } catch (SQLException | ClassNotFoundException e) {
            log.log(Level.SEVERE, e.toString(), e);
        }

        return dbConn;
    }

}
