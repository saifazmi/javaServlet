package users.login;

import encryption.SHA_512;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static users.database.SQLiteConnection.getConnection;

/**
 * @author : saif
 * @project : javaServlet
 * @date : 08/12/15
 */

/**
 * Represents a login authenticator
 */
public class AuthenticateUser {

    private static final Logger log = Logger.getLogger(AuthenticateUser.class.getName());
    private final String SEARCH = "SELECT userID, password" +
            " FROM LoginData" +
            " WHERE userID = ? AND password = ?;";

    private String userID;
    private String password;

    /**
     * Creates an authenticator
     *
     * @param userID   username to be authenticated.
     * @param password password to be authenticated.
     */
    public AuthenticateUser(String userID, String password) {
        this.userID = userID.toLowerCase();
        this.password = password;
    }

    /**
     * Validates the login
     *
     * @return true if the login credentials match in databse.
     */
    public boolean validLogin() {
        boolean valid = false;
        try (Connection dbConn = getConnection()) {

            PreparedStatement prepSmt = dbConn.prepareStatement(SEARCH);
            SHA_512 shaEncrypt = new SHA_512(userID, password);

            prepSmt.setString(1, userID);
            prepSmt.setString(2, shaEncrypt.getUserPasswordHash());

            try (ResultSet rs = prepSmt.executeQuery()) {

                valid = rs.next();
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
        }
        return valid;
    }
}
