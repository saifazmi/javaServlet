package encryption;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import static users.database.SQLiteConnection.getConnection;

/**
 * @author : saif
 * @project : javaServlet
 * @date : 11/12/15
 */

/**
 * Salt for encryption algorithms.
 */
public class Salt {

    private static final Logger LOG = Logger.getLogger(Salt.class.getName());
    private final String INSERT = "INSERT INTO " +
            " EncryptionSalts (userID, salt)" +
            " VALUES (?,?);";
    private final String SEARCH = "SELECT salt" +
            " FROM EncryptionSalts" +
            " WHERE userID = ?;";

    private String userID;

    /**
     * Initialises the encryption salt with required data.
     *
     * @param userID the user ID to which this salt belongs.
     */
    public Salt(String userID) {
        this.userID = userID;
    }

    /**
     * Generates a new random salt.
     *
     * @return a randomly generated salt string.
     */
    public String generateSalt() {
        String generatedSalt = null;
        try {
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            byte[] salt = new byte[16];
            sr.nextBytes(salt);
            generatedSalt = Arrays.toString(salt);
        } catch (NoSuchAlgorithmException e) {
            LOG.log(Level.SEVERE, e.toString(), e);
        }
        return generatedSalt;
    }

    /**
     * Gets the salt of an existing user.
     *
     * @return an existing salt string if the user exists, else a randomly generated salt string.
     */
    public String getUserSalt() {
        String salt = null;

        try (Connection dbConn = getConnection()) {
            PreparedStatement prepSmt = dbConn.prepareStatement(SEARCH);
            prepSmt.setString(1, userID);
            try (ResultSet rs = prepSmt.executeQuery()) {
                while (rs.next()) {
                    salt = rs.getString("salt");
                }

                if (salt == null) {
                    salt = generateSalt();
                }
            }
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, e.toString(), e);
        }
        return salt;
    }

    /**
     * Stores a given salt in the proper database.
     *
     * @param salt the salt string to store in the database.
     */
    public void storeUserSalt(String salt) {
        int numberOfSaltStored;
        try (Connection dbConn = getConnection()) {

            PreparedStatement prepSmt = dbConn.prepareStatement(INSERT);
            prepSmt.setString(1, userID);
            prepSmt.setString(2, salt);
            numberOfSaltStored = prepSmt.executeUpdate();

            if (numberOfSaltStored > 0) {
                LOG.log(Level.INFO, Integer.toString(numberOfSaltStored).concat(" salt stored."));
            } else {
                LOG.log(Level.SEVERE, "Error while storing user encryption salt");
            }
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, e.toString(), e);
        }
    }
}
