package users.register;

import encryption.SHA_512;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static users.database.SQLiteConnection.getConnection;

/**
 * @author : saif
 * @project : javaServlet
 * @date : 08/12/15
 */

/**
 * Represents a new user.
 */
public class NewUser {

    private static final Logger log = Logger.getLogger(NewUser.class.getName());
    private final String INSERT = "INSERT INTO " +
            " LoginData (userID, password)" +
            " VALUES (?,?);";

    final String FIND = "SELECT userID" +
            " FROM LoginData" +
            " WHERE userID = ?;";

    private String userID;
    private String password;

    /**
     * Initialises a new user
     *
     * @param userID   username to be created for login.
     * @param password password to be created for login.
     */
    public NewUser(String userID, String password) {
        this.userID = userID.toLowerCase();
        this.password = password;
    }

    /**
     * Creates a new user in the databse.
     *
     * @return 1 if user was created else 0.
     */
    public int create() {

        int numberOfUserCreated = 0;
        try (Connection dbConn = getConnection()) {
            PreparedStatement prepStmt = dbConn.prepareStatement(INSERT);
            SHA_512 shaEncrypt = new SHA_512(userID, password);
            prepStmt.setString(1, userID);
            prepStmt.setString(2, shaEncrypt.generateSecurePassword());
            numberOfUserCreated = prepStmt.executeUpdate();

            if (numberOfUserCreated > 0) {
                log.log(Level.INFO, numberOfUserCreated + " user created");
            } else {
                log.log(Level.SEVERE, "Error while creating user");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
        }

        return numberOfUserCreated;
    }

    /**
     * Checks if the user already exists in the database.
     *
     * @return true if the user already exists else false.
     */
    public boolean checkExistingUser() {

        boolean existing = true;

        try (Connection dbConn = getConnection()) {

            PreparedStatement prepSmt = dbConn.prepareStatement(FIND);
            prepSmt.setString(1, userID);
            try (ResultSet rs = prepSmt.executeQuery()) {

                existing = rs.next();
            }

        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
        }

        return existing;
    }
}
