package email;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import static email.SMTPSession.getSMTPSession;

/**
 * @author : saif
 * @project : javaMail
 * @date : 12/11/15
 */

/**
 * Represents an outbound email.
 */
public class SendEmail {

    private static final Logger log = Logger.getLogger(SendEmail.class.getName());

    /**
     * Constructs a new email.
     *
     * @param fromAddress    senders email address.
     * @param password       senders email password.
     * @param toAddresses    recipients email addresses.
     * @param ccAddresses    carbon copy email addresses.
     * @param messageSubject subject of the message.
     * @param messageBody    body of the message.
     * @param attachments    attachment file.
     */
    public SendEmail(String fromAddress, String password, ArrayList<String> toAddresses, ArrayList<String> ccAddresses, String messageSubject, String messageBody, ArrayList<File> attachments) {

        try {
            MimeMessage message = new MimeMessage(getSMTPSession(fromAddress, password));
            message.setFrom(new InternetAddress(fromAddress));
            for (String toAddress : toAddresses) {  // loops through the recipients emails
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddress));
            }

            for (String ccAddress : ccAddresses) {  // loops through the cc emails
                message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccAddress));
            }
            message.setSubject(messageSubject); // adds the subject

            // creating a multipart body for message body and attachments.
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText(messageBody);
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            // adds attachments
            if (attachments != null && attachments.size() > 0) {

                for (File attachment : attachments) {
                    messageBodyPart = new MimeBodyPart();
                    String path = attachment.getAbsolutePath();
                    DataSource source = new FileDataSource(path);
                    messageBodyPart.setDataHandler(new DataHandler(source));
                    messageBodyPart.setFileName(attachment.getName());
                    multipart.addBodyPart(messageBodyPart);
                    log.log(Level.INFO, "Attached File: ".concat(attachment.getName()));
                }
            }

            message.setContent(multipart);

            message.saveChanges();

            // creating a transporter to send the message.
            Transport tr = getSMTPSession(fromAddress, password).getTransport("smtp");
            tr.connect("smtp.gmail.com",
                    fromAddress,
                    password);
            tr.sendMessage(message, message.getAllRecipients());

            log.log(Level.INFO, "Message Sent");
        } catch (MessagingException e) {
            log.log(Level.SEVERE, e.toString(), e);
        }
    }

    /**
     * Helper function to format the string of emails into well formed lists.
     *
     * @param addresses emails to be formatted.
     * @return a list of emails.
     */
    public static ArrayList<String> createRecepientList(String addresses) {

        // removing spaces, newline, tabs, comma and semicolons.
        String regex = "[\\s,;\\n\\t]+";
        String[] tokens = addresses.split(regex);
        ArrayList<String> addressList = new ArrayList<>(Arrays.asList(tokens));

        return addressList;
    }
}
