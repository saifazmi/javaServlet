package users.register;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author : saif
 * @project : javaServlet
 * @date : 08/12/15
 */
@WebServlet(name = "ServletRegisterUser", value = "/register")
public class ServletRegisterUser extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String newUserName = request.getParameter("newUser");
        String newPassword = request.getParameter("newPass");
        String newPassCheck = request.getParameter("newPassCheck");

        NewUser newUser = new NewUser(newUserName, newPassword);

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        if (newUser.checkExistingUser()) {
            // if user already exists.
            request.setAttribute("username", newUserName);

            // redirect to registration page with error message.
            out.println("<p><font color=red> User already exists." +
                    "<br/>Please try to login or use a different user name.</font></p>");
            RequestDispatcher rd = request.getRequestDispatcher("/register.jsp");
            rd.include(request, response);

        } else if (!newPassword.equals(newPassCheck)) {
            // if passwords dont match.
            request.setAttribute("username", newUserName);

            // redirect to registration page with error message.
            out.println("<p><font color=red> Passwords don't match</font></p>");
            RequestDispatcher rd = request.getRequestDispatcher("/register.jsp");
            rd.include(request, response);
        } else {
            // create new user
            newUser.create();
            // redirect to confirmation page.
            RequestDispatcher rd = request.getRequestDispatcher("/registrationConfirmation.jsp");
            rd.forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
