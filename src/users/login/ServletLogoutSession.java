package users.login;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author : saif
 * @project : javaServlet
 * @date : 08/12/15
 */

/**
 * Servlet to control the logout functionality.
 */
@WebServlet(name = "ServletLogoutSession", value = "/logout")
public class ServletLogoutSession extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // end the session on logout request.
        request.getSession(false).invalidate();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        // redirect to login page with confirmation message.
        out.println("<p><font color=green> You have been successfully logged out.</font></p>");
        RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
        rd.include(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
