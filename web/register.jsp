<%--
  Created by IntelliJ IDEA.
  User: saif
  Date: 08/12/15
  Time: 02:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
</head>
<body>
<h1>JavaServlet Email Sender | Registration</h1>

<h3>Please Enter details to register -</h3>

<form action="register" method="POST">
    <%
        String user;
        if (request.getAttribute("username") != null) {
            user = (String) request.getAttribute("username");
        } else {
            user = "";
        }
    %>
    <p>Username: <input type="text" value="<%=user%>" name="newUser" size="50" required="required"></p>

    <p>Pasword: <input type="password" name="newPass" size="50" required="required"></p>

    <p>Re-enter Pasword: <input type="password" name="newPassCheck" size="50" required="required"></p>

    <p><input type="submit" value="Register"></p>
</form>
</body>
</html>
