<%--
  Created by IntelliJ IDEA.
  User: saif
  Date: 06/12/15
  Time: 19:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Login | Email Send</title>
</head>
<body>
<main>
    <h1>JavaServlet Email Sender | Login</h1>

    <h3>Please Enter login details below -</h3>

    <form action="login" method="POST">
        <%
            String user;
            if (request.getAttribute("username") != null) {
                user = (String) request.getAttribute("username");
            } else {
                user = "";
            }
        %>
        <p>Username: <input type="text" value="<%=user%>" name="username" size="50" required="required"></p>

        <p>Pasword: <input type="password" name="password" size="50" required="required"></p>

        <p><input type="submit" value="Login"></p>
    </form>
    <a href="register.jsp">Register</a>
</main>
</body>
</html>
