package encryption;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : javaServlet
 * @date : 11/12/15
 */

/**
 * An SHA-512 encryption algorithm.
 */
public class SHA_512 {

    private static final Logger LOG = Logger.getLogger(SHA_512.class.getName());

    private String password;
    private Salt encSalt;

    /**
     * Initialises the encryption with required data.
     *
     * @param userID
     * @param password
     */
    public SHA_512(String userID, String password) {
        this.password = password;
        this.encSalt = new Salt(userID);
    }

    /**
     * Generates a new SHA-512 hash for the given password with a new salt.
     *
     * @return a 512-bits hash of the given password.
     */
    public String generateSecurePassword() {
        // Get the random salt.
        String salt = encSalt.generateSalt();
        // Store the salt in the database for login checks.
        encSalt.storeUserSalt(salt);
        return hash(password, salt);
    }

    /**
     * Generates the SHA-512 hash for the given password with the existing user salt.
     *
     * @return a 512-bits hash of the given password.
     */
    public String getUserPasswordHash() {
        // Get the existing salt for user from the database.
        String salt = encSalt.getUserSalt();
        return hash(password, salt);
    }

    /**
     * Generates a SHA-512 hash for the given data.
     *
     * @param toHash data to be hashed.
     * @param salt   additional string to append to the data.
     * @return a 512-bits hash of the given string.
     */
    private String hash(String toHash, String salt) {
        String hashedString = null;

        try {
            // Create message digest instance for SHA-512
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            // Add salt bytes to digest.
            md.update(salt.getBytes());
            // Get the hash bytes.
            byte[] bytes = md.digest(toHash.getBytes());
            // This bytes[] has bytes in decimal format;
            // Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            // Get complete hashed password in hex format
            hashedString = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            LOG.log(Level.SEVERE, e.toString(), e);
        }

        return hashedString;
    }
}
