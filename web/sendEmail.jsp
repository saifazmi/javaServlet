<%--
  Created by IntelliJ IDEA.
  User: saif
  Date: 07/12/15
  Time: 18:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Send Email</title>
</head>
<body>
<main>
    <h1>Email Sender</h1>

    <form action="sendemail" method="POST" enctype="multipart/form-data">
        <p>To: <input type="email" name="toAddress" size="50" required="required"></p>

        <p>Cc: <input type="email" name="ccAddress" size="50"></p>

        <p>Subject: <input type="text" name="subject" size="50" required="required"></p>

        <p><textarea rows="25" cols="60" name="messageBody"></textarea></p>

        <p>Attachments: <input type="file" name="attachments" multiple size="50"></p>

        <p><input type="submit" value="Send"></p>
    </form>
    <br/>

    <form action="logout" method="POST">
        <p><input type="submit" value="Logout"></p>
    </form>
</main>
</body>
</html>
