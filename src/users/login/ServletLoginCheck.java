package users.login;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author : saif
 * @project : javaServlet
 * @date : 07/12/15
 */

/**
 * Servlet to control the login functionality.
 */
@WebServlet(name = "ServletLoginCheck", value = "/login")
public class ServletLoginCheck extends HttpServlet {

    private final int MAX_INTERVAL = 5 * 60;    // session timeout (5 minutes)

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // fetch username and password from the form
        String user = request.getParameter("username");
        String password = request.getParameter("password");

        AuthenticateUser auth = new AuthenticateUser(user, password);

        // check if login credentials match
        if (auth.validLogin()) {
            HttpSession session = request.getSession(true);
            // set session timeout limit.
            session.setAttribute("username", user);
            session.setAttribute("pass", password);
            session.setMaxInactiveInterval(MAX_INTERVAL);

            // redirect to send email page.
            RequestDispatcher rd = request.getRequestDispatcher("/sendEmail.jsp");
            rd.forward(request, response);
        } else {
            // if login fails.
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            request.setAttribute("username", user);

            // redirect to login page with error message.
            out.println("<p><font color=red> Username or Password do not match!" +
                    "<br/>Please try again or Register</font></p>");
            RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
            rd.include(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
