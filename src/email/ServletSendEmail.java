package email;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : javaServlet
 * @date : 07/12/15
 */

/**
 * Servlet to control the email send functionality.
 */
@WebServlet(name = "ServletSendEmail", value = "/sendemail")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2,   // 2MB
        maxFileSize = 1024 * 1024 * 10,         // 10MB
        maxRequestSize = 1024 * 1024 * 50)      // 50MB
public class ServletSendEmail extends HttpServlet {
    private static final Logger log = Logger.getLogger(ServletSendEmail.class.getName());

    private ArrayList<String> toAddress;
    private ArrayList<String> ccAddress;
    private ArrayList<File> attachemnts;

    /**
     * Initialises the required data structures.
     *
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        toAddress = new ArrayList<>();
        ccAddress = new ArrayList<>();
        attachemnts = new ArrayList<>();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(false);
        // check if the session is still valid i.e. it has not timed out.
        if (session != null) {

            // fetch the required data from the form
            String toEmails = request.getParameter("toAddress");
            String ccEmails = request.getParameter("ccAddress");
            String subject = request.getParameter("subject");
            String messageBody = request.getParameter("messageBody");

            // generate list of recipients
            toAddress = SendEmail.createRecepientList(toEmails);
            ccAddress = SendEmail.createRecepientList(ccEmails);

            generateAttachmentList(request);

            String username = (String) session.getAttribute("username");
            String pass = (String) session.getAttribute("pass");

            // create and send and email.
            new SendEmail(username,
                    pass,
                    toAddress,
                    ccAddress,
                    subject,
                    messageBody,
                    attachemnts);
            deleteUploadFiles(attachemnts);
            attachemnts.clear();

            // Redirect to the notification page
            RequestDispatcher rd = request.getRequestDispatcher("/sendNotification.jsp");
            rd.include(request, response);
        } else {
            // if the session expires before sending the email.
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            // redirect to login page with error message.
            out.println("<p><font color=blue> Session Expired!, Please login again.</font></p>");
            RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
            rd.include(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    /**
     * Saves files uploaded from the client and return a list of these files
     * which will be attached to the e-mail message.
     *
     * @param request the http request object
     */
    private ArrayList<File> generateAttachmentList(HttpServletRequest request) throws IOException, ServletException {

        ArrayList<File> filesToAttach = new ArrayList<>();
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        Collection<Part> multiparts = request.getParts();
        if (multiparts.size() > 0) {
            for (Part part : request.getParts()) {
                // creates a file to be saved
                String fileName = extractFileName(part);
                if (!(fileName == null || fileName.equals(""))) {
                    File saveFile = new File(fileName);
                    log.log(Level.INFO, "Uploaded: " + saveFile.getAbsolutePath());
                    try (FileOutputStream outputStream = new FileOutputStream(saveFile)) {
                        // saves uploaded file
                        try (InputStream inputStream = part.getInputStream()) {
                            while ((bytesRead = inputStream.read(buffer)) != -1) {
                                outputStream.write(buffer, 0, bytesRead);
                            }
                        }
                    } catch (IOException e) {
                        log.log(Level.SEVERE, e.toString(), e);
                    }
                    attachemnts.add(saveFile);
                }
            }
        }
        return filesToAttach;
    }

    /**
     * Retrieves file name of a upload part from its HTTP header
     *
     * @param part part object containing the file
     */
    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return null;
    }

    /**
     * Deletes all uploaded files, should be called after the e-mail was sent.
     *
     * @param listFiles list of files to be deleted.
     */
    private void deleteUploadFiles(ArrayList<File> listFiles) {
        if (listFiles != null && listFiles.size() > 0) {
            for (File aFile : listFiles) {
                aFile.delete();
            }
        }
    }
}
