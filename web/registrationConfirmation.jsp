<%--
  Created by IntelliJ IDEA.
  User: saif
  Date: 08/12/15
  Time: 08:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registered!</title>
</head>
<body>
<h2>You have been registered successfully!</h2>

<p>
    <a href="index.jsp">Login now</a> with your new credentials.
</p>
</body>
</html>
